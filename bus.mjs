export default function busBuilder() {

  const eventsListeners = {}

  return {
    send(eventName, payload) {
      const listeners = eventsListeners[eventName] || []
      listeners.forEach(listenerCompound => {
        listenerCompound.listener(payload)
        if (listenerCompound.once) {
          this.off(eventName, listenerCompound.listener)
        }
      })
    },

    on(eventName, listener, options) {
      if (!eventsListeners[eventName]) eventsListeners[eventName] = []
      eventsListeners[eventName].push({ listener, ...options })
      return listener
    },

    once(eventName, listener) {
      this.on(eventName, listener, { once: true })
    },

    /**
     * Remove listener from event
     * If it was removed returns true, otherwise returns false.
     */
    off(eventName, listener) {
      if (!eventsListeners[eventName]) return false
      const len = eventsListeners[eventName].length
      eventsListeners[eventName] = eventsListeners[eventName]
                                   .filter(l => l.listener !== listener)
      return eventsListeners[eventName].length !== len
    },

    listeners(eventName) {
      const evListeners = eventsListeners[eventName] || []
      return [...evListeners]
    }
  }

}

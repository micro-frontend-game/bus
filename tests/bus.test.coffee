describe 'Bus', ->

  it 'builds', ->
    bus.should.deep.keys(['on', 'once', 'off', 'send', 'listeners'])

  it 'registers an unlimited event listener.', ->
    listener = -> console.log 'hello'
    bus.listeners('any_event').should.be.empty
    bus.on 'any_event', listener
    bus.listeners('any_event').should.deep.include { listener }

  it 'registers an event for one time run', ->
    listener = -> console.log 'once'
    bus.listeners('any_event').should.be.empty
    bus.once 'any_event', listener
    bus.listeners('any_event').should.deep.include { listener, once: true }

  it 'listens for an event only once', (done)->
    counter = 0
    listener = -> counter++
    bus.listeners('any_event').should.be.empty
    bus.once 'any_event', listener
    setTimeout (-> bus.send 'any_event'), 30
    setTimeout (->
      bus.listeners('any_event').should.be.empty
      bus.send 'any_event'
    ), 60
    setTimeout (->
      counter.should.equal 1
      done()
    ), 90

  it 'removes a listener', ->
    listener = -> console.log 'hello'
    bus.on 'any_event', listener
    bus.listeners('any_event').should.deep.include { listener }
    removed = bus.off 'any_event', listener
    removed.should.be.true
    bus.listeners('any_event').should.be.empty

  it 'tries to remove a non-existant listener', ->
    listener = -> console.log 'goodbye'
    removed = bus.off 'any_event', listener
    removed.should.be.false

  it 'sends an event (without payload) that is listened', ->
    flag = false
    listener = -> flag = true
    bus.on 'any_event', listener
    bus.send 'any_event'
    flag.should.be.true

  it 'sends an event (without payload) that is asynchronously listened', (done)->
    listener = done
    bus.on 'any_event', listener
    setTimeout (-> bus.send 'any_event'), 100

  it 'sends an event (with payload) that is asynchronously listened', (done)->
    listener = (payload)->
      payload.foo.should.be.equal 'bar'
      done()
    bus.on 'any_event', listener
    setTimeout (-> bus.send 'any_event', {foo: 'bar'}), 100

  it 'sends many asynchronous events', (done)->
    counterA = 0
    counterB = 0
    listenerA = -> counterA++
    listenerB = -> counterB++
    bus.on 'eventA', listenerA
    bus.on 'eventB', listenerB
    setTimeout (-> bus.send 'eventA'), 30
    setTimeout (-> bus.send 'eventB'), 60
    setTimeout (-> bus.send 'eventA'), 90
    setTimeout (->
      counterA.should.be.equal 2
      counterB.should.be.equal 1
      done()
    ), 120

export const mochaHooks = {

  async beforeAll() {
    global.buildBus = (await import('../bus.mjs?t=' + Date.now())).default
  },

  beforeEach() {
    global.bus = buildBus()
  }

}
